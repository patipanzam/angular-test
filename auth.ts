// Simulated API call
export const login = async (username: string, password: string) => {
  if (username === 'user' && password === 'password') {
    return { token: 'mock-token' }; // Mock token
  } else {
    throw new Error('Invalid credentials');
  }
};
