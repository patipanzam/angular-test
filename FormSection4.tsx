import React, { useEffect, useMemo, useState } from 'react';
import { StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { Divider } from 'react-native-paper';
import { useFormContext } from '../contexts/FormContext';
import Theme from '../Theme';
import TextInput from './TextInput';
import VerifyFormHeader from './VerifyFormHeader';
import numeral from 'numeral';
import { LOG } from '../utils/Logs';
import Utils from '../utils/Utils';

const FormSection4: React.FC = () => {
  const { form, setFormValue, error } = useFormContext();

  useEffect(() => {
    console.log('--- section4 ---');
    LOG.debug('--- section4 ---');
    for (const key of Object.keys(form.section4)) {
      console.log(key, ' => ', form.section4?.[key]);
      LOG.debug(key, ' => ', form.section4?.[key]);
    }
    console.log('--- end section4 ---');
    LOG.debug('--- end section4 ---');
  }, [form.section4]);

  const sum = (value1: string, value2: string, value3: string) => {
    return (
      Number(value1.replace(/,/g, '')) +
      Number(value2.replace(/,/g, '')) +
      Number(value3.replace(/,/g, ''))
    );
  };

  const sumOutcome1 = () => {
    const { A12_C1_2, A12_C2_2, A12_C3_2 } = form.section4;
    return sum(A12_C1_2, A12_C2_2, A12_C3_2);
  };

  const sumOutcome2 = () => {
    const { A16_C1_2, A16_C2_2, A16_C3_2 } = form.section4;
    return sum(A16_C1_2, A16_C2_2, A16_C3_2);
  };

  const sumOutcome3 = () => {
    const { A18_C1_2, A18_C2_2, A18_C3_2 } = form.section4;
    return sum(A18_C1_2, A18_C2_2, A18_C3_2);
  };

  const totalOutcome = () => {
    return sumOutcome1() + sumOutcome2() + sumOutcome3();
  };

  const totalIncome = () => {
    return (
      Number(form.section4.A11_C1.replace(/,/g, '')) +
      Number(form.section4.A15_C1.replace(/,/g, '')) +
      Number(form.section4.A17_C1.replace(/,/g, ''))
    );
  };

  const SumIncomeOutcome1 = useMemo(() => {
    return (
      <SumIncomeOutcome
        netIncome={
          Number(form.section4.A11_C1.replace(/,/g, '')) - sumOutcome1()
        }
        outcome={sumOutcome1()}
      />
    );
  }, [
    form.section4.A12_C1_2,
    form.section4.A12_C2_2,
    form.section4.A12_C3_2,
    form.section4.A11_C1,
  ]);

  const SumIncomeOutcome2 = useMemo(() => {
    return (
      <SumIncomeOutcome
        netIncome={
          Number(form.section4.A15_C1.replace(/,/g, '')) - sumOutcome2()
        }
        outcome={sumOutcome2()}
      />
    );
  }, [
    form.section4.A16_C1_2,
    form.section4.A16_C2_2,
    form.section4.A16_C3_2,
    form.section4.A15_C1,
  ]);

  const SumIncomeOutcome3 = useMemo(() => {
    return (
      <SumIncomeOutcome
        netIncome={
          Number(form.section4.A17_C1.replace(/,/g, '')) - sumOutcome3()
        }
        outcome={sumOutcome3()}
      />
    );
  }, [
    form.section4.A18_C1_2,
    form.section4.A18_C2_2,
    form.section4.A18_C3_2,
    form.section4.A17_C1,
  ]);

  return (
    <>
      <VerifyFormHeader title="รายละเอียดรายได้และกิจการ (โปรดระบุข้อมูลลูกค้า)" />
      <View style={[styles.formMargin, { marginLeft: 30 }]}>
        {form.section4.incomeFormQTY >= 1 && (
          <>
            <Text>รายละเอียดรายได้/รายรับในกิจการที่ 1</Text>
            <TextInput
              inputStyle={{
                backgroundColor: Theme.WHITE_COLOR,
                color: Theme.BLACK_COLOR,
              }}
              disableFullscreenUI={true}
              label="ชื่อกิจการ"
              isRequired
              value={form.section4.A19}
              onChangeText={text => setFormValue('section4', 'A19', text)}
              isError={!!error?.['section4.A19']}
              caption={error?.['section4.A19']}
            />
            <View style={{ width: '50%' }}>
              <TextInput
                inputStyle={{
                  backgroundColor: Theme.WHITE_COLOR,
                  color: Theme.BLACK_COLOR,
                }}
                disableFullscreenUI={true}
                label="ยอดขายรายได้ (บาท/เดือน)"
                placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                placeholderTextColor="gray"
                isRequired
                keyboardType="decimal-pad"
                autoCapitalize="words"
                value={form.section4.A11_C1}
                onChangeText={text => {
                  const validateText = Utils.handleOnChangeTextForDecimal(text);
                  if (validateText) {
                    const newText = Utils.isDecimalTwoPosition(text);
                    setFormValue(
                      'section4',
                      'A11_C1',
                      Utils.hasCheckFullStopCharacters(newText),
                    );
                  }
                }}
                onEndEditing={() => {
                  if (form.section4.A11_C1) {
                    setFormValue(
                      'section4',
                      'A11_C1',
                      numeral(form.section4.A11_C1).format('0,0.00'),
                    );
                  }
                }}
                isError={!!error?.['section4.A11_C1']}
                caption={error?.['section4.A11_C1']}
              />
            </View>
            <Text style={{ marginVertical: 10 }}>รายละเอียดต้นทุน</Text>
            <View style={{ marginLeft: 15 }}>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>1.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  isRequired
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A12_C1_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A12_C1_1', text)
                  }
                  isError={!!error?.['section4.A12_C1_1']}
                  caption={error?.['section4.A12_C1_1']}
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  isRequired
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A12_C1_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A12_C1_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A12_C1_2) {
                      setFormValue(
                        'section4',
                        'A12_C1_2',
                        numeral(form.section4.A12_C1_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A12_C1_2']}
                  caption={error?.['section4.A12_C1_2']}
                />
              </View>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>2.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A12_C2_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A12_C2_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A12_C2_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A12_C2_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A12_C2_2) {
                      setFormValue(
                        'section4',
                        'A12_C2_2',
                        numeral(form.section4.A12_C2_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A12_C2_2']}
                  caption={error?.['section4.A12_C2_2']}
                />
              </View>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>3.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A12_C3_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A12_C3_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A12_C3_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A12_C3_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A12_C3_2) {
                      setFormValue(
                        'section4',
                        'A12_C3_2',
                        numeral(form.section4.A12_C3_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A12_C3_2']}
                  caption={error?.['section4.A12_C3_2']}
                />
              </View>
            </View>

            {SumIncomeOutcome1}
          </>
        )}

        {form.section4.incomeFormQTY >= 2 && (
          <>
            <Divider style={{ height: 1.5, marginBottom: 20 }} />
            <Text>รายละเอียดรายได้/รายรับในกิจการที่ 2 (ถ้ามี)</Text>
            <TextInput
              inputStyle={{
                backgroundColor: Theme.WHITE_COLOR,
                color: Theme.BLACK_COLOR,
              }}
              disableFullscreenUI={true}
              label="ชื่อกิจการ"
              value={form.section4.A20}
              onChangeText={text => setFormValue('section4', 'A20', text)}
            />
            <View style={{ width: '50%' }}>
              <TextInput
                inputStyle={{
                  backgroundColor: Theme.WHITE_COLOR,
                  color: Theme.BLACK_COLOR,
                }}
                disableFullscreenUI={true}
                label="ยอดขายรายได้ (บาท/เดือน)"
                placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                placeholderTextColor="gray"
                keyboardType="decimal-pad"
                autoCapitalize="words"
                value={form.section4.A15_C1}
                onChangeText={text => {
                  const validateText = Utils.handleOnChangeTextForDecimal(text);
                  if (validateText) {
                    const newText = Utils.isDecimalTwoPosition(text);
                    setFormValue(
                      'section4',
                      'A15_C1',
                      Utils.hasCheckFullStopCharacters(newText),
                    );
                  }
                }}
                onEndEditing={() => {
                  if (form.section4.A15_C1) {
                    setFormValue(
                      'section4',
                      'A15_C1',
                      numeral(form.section4.A15_C1).format('0,0.00'),
                    );
                  }
                }}
                isError={!!error?.['section4.A15_C1']}
                caption={error?.['section4.A15_C1']}
              />
            </View>
            <Text style={{ marginVertical: 10 }}>รายละเอียดต้นทุน</Text>
            <View style={{ marginLeft: 15 }}>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>1.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A16_C1_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A16_C1_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A16_C1_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A16_C1_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A16_C1_2) {
                      setFormValue(
                        'section4',
                        'A16_C1_2',
                        numeral(form.section4.A16_C1_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A16_C1_2']}
                  caption={error?.['section4.A16_C1_2']}
                />
              </View>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>2.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A16_C2_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A16_C2_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A16_C2_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A16_C2_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A16_C2_2) {
                      setFormValue(
                        'section4',
                        'A16_C2_2',
                        numeral(form.section4.A16_C2_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A16_C2_2']}
                  caption={error?.['section4.A16_C2_2']}
                />
              </View>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>3.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A16_C3_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A16_C3_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A16_C3_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A16_C3_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A16_C3_2) {
                      setFormValue(
                        'section4',
                        'A16_C3_2',
                        numeral(form.section4.A16_C3_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A16_C3_2']}
                  caption={error?.['section4.A16_C3_2']}
                />
              </View>
            </View>

            {SumIncomeOutcome2}
          </>
        )}

        {form.section4.incomeFormQTY === 3 && (
          <>
            <Divider style={{ height: 1.5, marginBottom: 20 }} />
            <Text>รายละเอียดรายได้/รายรับในกิจการที่ 3 (ถ้ามี)</Text>
            <TextInput
              inputStyle={{
                backgroundColor: Theme.WHITE_COLOR,
                color: Theme.BLACK_COLOR,
              }}
              disableFullscreenUI={true}
              label="ชื่อกิจการ"
              value={form.section4.A21}
              onChangeText={text => setFormValue('section4', 'A21', text)}
            />
            <View style={{ width: '50%' }}>
              <TextInput
                inputStyle={{
                  backgroundColor: Theme.WHITE_COLOR,
                  color: Theme.BLACK_COLOR,
                }}
                disableFullscreenUI={true}
                label="ยอดขายรายได้ (บาท/เดือน)"
                placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                placeholderTextColor="gray"
                keyboardType="decimal-pad"
                autoCapitalize="words"
                value={form.section4.A17_C1}
                onChangeText={text => {
                  const validateText = Utils.handleOnChangeTextForDecimal(text);
                  if (validateText) {
                    const newText = Utils.isDecimalTwoPosition(text);
                    setFormValue(
                      'section4',
                      'A17_C1',
                      Utils.hasCheckFullStopCharacters(newText),
                    );
                  }
                }}
                onEndEditing={() => {
                  if (form.section4.A17_C1) {
                    setFormValue(
                      'section4',
                      'A17_C1',
                      numeral(form.section4.A17_C1).format('0,0.00'),
                    );
                  }
                }}
                isError={!!error?.['section4.A17_C1']}
                caption={error?.['section4.A17_C1']}
              />
            </View>
            <Text style={{ marginVertical: 10 }}>รายละเอียดต้นทุน</Text>
            <View style={{ marginLeft: 15 }}>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>1.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A18_C1_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A18_C1_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A18_C1_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A18_C1_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A18_C1_2) {
                      setFormValue(
                        'section4',
                        'A18_C1_2',
                        numeral(form.section4.A18_C1_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A18_C1_2']}
                  caption={error?.['section4.A18_C1_2']}
                />
              </View>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>2.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A18_C2_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A18_C2_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A18_C2_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A18_C2_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A18_C2_2) {
                      setFormValue(
                        'section4',
                        'A18_C2_2',
                        numeral(form.section4.A18_C2_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A18_C2_2']}
                  caption={error?.['section4.A18_C2_2']}
                />
              </View>
              <View style={styles.row}>
                <Text style={{ marginTop: 12 }}>3.</Text>
                <TextInput
                  disableFullscreenUI={true}
                  label="ต้นทุน"
                  placeholder="ต้นทุน"
                  placeholderTextColor="gray"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A18_C3_1}
                  onChangeText={text =>
                    setFormValue('section4', 'A18_C3_1', text)
                  }
                />
                <TextInput
                  disableFullscreenUI={true}
                  label="จำนวน (บาท/เดือน)"
                  placeholder="กรอกเฉพาะตัวเลข หรือตัวเลขพร้อมกับจุดทศนิยม"
                  placeholderTextColor="gray"
                  keyboardType="decimal-pad"
                  autoCapitalize="words"
                  textAlign="right"
                  inputStyle={{
                    marginHorizontal: 5,
                    backgroundColor: Theme.WHITE_COLOR,
                    color: Theme.BLACK_COLOR,
                  }}
                  value={form.section4.A18_C3_2}
                  onChangeText={text => {
                    const validateText =
                      Utils.handleOnChangeTextForDecimal(text);
                    if (validateText) {
                      const newText = Utils.isDecimalTwoPosition(text);
                      setFormValue(
                        'section4',
                        'A18_C3_2',
                        Utils.hasCheckFullStopCharacters(newText),
                      );
                    }
                  }}
                  onEndEditing={() => {
                    if (form.section4.A18_C3_2) {
                      setFormValue(
                        'section4',
                        'A18_C3_2',
                        numeral(form.section4.A18_C3_2).format('0,0.00'),
                      );
                    }
                  }}
                  isError={!!error?.['section4.A18_C3_2']}
                  caption={error?.['section4.A18_C3_2']}
                />
              </View>
            </View>

            {SumIncomeOutcome3}
            <Divider style={{ height: 1.5 }} />
          </>
        )}

        {form.section4.incomeFormQTY < 3 && (
          <>
            <TouchableHighlight
              style={{
                padding: 10,
                backgroundColor: Theme.GRAY_COLOR,
                maxWidth: 250,
              }}
              underlayColor={Theme.GRAY_BORDER_COLOR}
              onPress={() =>
                setFormValue(
                  'section4',
                  'incomeFormQTY',
                  ++form.section4.incomeFormQTY,
                )
              }>
              <Text style={{ textAlign: 'center' }}>
                + เพิ่มข้อมูลรายได้/ต้นทุนกิจการ
              </Text>
            </TouchableHighlight>
            <Divider style={{ height: 1.5, marginVertical: 20 }} />
          </>
        )}

        <SumAllIncomeOutcome
          totalNetIncome={totalIncome() - totalOutcome()}
          totalOutcome={totalOutcome()}
          totalIncome={totalIncome()}
        />

        <TextInput
          inputStyle={{
            backgroundColor: Theme.WHITE_COLOR,
            color: Theme.BLACK_COLOR,
          }}
          disableFullscreenUI={true}
          label="ข้อมูลเพิ่มเติม"
          multiline={true}
          maxLength={2000}
          value={form.section4.A13}
          onChangeText={text => setFormValue('section4', 'A13', text)}
        />
        <TextInput
          inputStyle={{
            backgroundColor: Theme.WHITE_COLOR,
            color: Theme.BLACK_COLOR,
          }}
          disableFullscreenUI={true}
          label="ข้อมูลลูกค้า (โปรดระบุ)"
          isRequired
          multiline={true}
          maxLength={2000}
          value={form.section4.A14_C1}
          onChangeText={text => setFormValue('section4', 'A14_C1', text)}
          isError={!!error?.['section4.A14_C1']}
          caption={error?.['section4.A14_C1']}
        />
        <TextInput
          inputStyle={{
            backgroundColor: Theme.WHITE_COLOR,
            color: Theme.BLACK_COLOR,
          }}
          disableFullscreenUI={true}
          label="ข้อมูลข้างเคียง"
          multiline={true}
          maxLength={2000}
          value={form.section4.A14_C2}
          onChangeText={text => setFormValue('section4', 'A14_C2', text)}
        />
        <TextInput
          inputStyle={{
            backgroundColor: Theme.WHITE_COLOR,
            color: Theme.BLACK_COLOR,
          }}
          disableFullscreenUI={true}
          label="ข้อมูลรายได้"
          multiline={true}
          maxLength={2000}
          value={form.section4.A14_C3}
          onChangeText={text => setFormValue('section4', 'A14_C3', text)}
        />
      </View>
    </>
  );
};

interface ISumIncomeOutcomeProps {
  netIncome: number;
  outcome: number;
}

const SumIncomeOutcome = ({ netIncome, outcome }: ISumIncomeOutcomeProps) => {
  return (
    <View style={{ marginVertical: 20 }}>
      <View style={styles.summaryContainer}>
        <Text style={styles.summaryTitle}>รวมรายจ่าย</Text>
        <View style={styles.summaryValueContainer}>
          <Text style={styles.summaryValue}>
            {numeral(outcome).format('0,0.00')}
          </Text>
          <Text>บาท</Text>
        </View>
      </View>
      <View style={[styles.summaryContainer, { marginTop: 20 }]}>
        <Text style={styles.summaryTitle}>รายได้สุทธิ</Text>
        <View style={styles.summaryValueContainer}>
          <Text style={styles.summaryValue}>
            {numeral(netIncome).format('0,0.00')}
          </Text>
          <Text>บาท</Text>
        </View>
      </View>
    </View>
  );
};

interface ISumAllIncomeOutcomeProps {
  totalNetIncome: number;
  totalOutcome: number;
  totalIncome: number;
}

const SumAllIncomeOutcome = ({
  totalNetIncome,
  totalOutcome,
  totalIncome,
}: ISumAllIncomeOutcomeProps) => {
  return (
    <>
      <View style={{ marginVertical: 20 }}>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryTitle}>รวมรายได้ทุกกิจการ</Text>
          <View style={styles.summaryValueContainer}>
            <Text
              style={[
                styles.summaryValue,
                { backgroundColor: Theme.GRAY_COLOR },
              ]}>
              {numeral(totalIncome).format('0,0.00')}
            </Text>
            <Text>บาท</Text>
          </View>
        </View>
        <View style={[styles.summaryContainer, { marginVertical: 20 }]}>
          <Text style={styles.summaryTitle}>รวมรายจ่ายทุกกิจการ</Text>
          <View style={styles.summaryValueContainer}>
            <Text
              style={[
                styles.summaryValue,
                { backgroundColor: Theme.GRAY_COLOR },
              ]}>
              {numeral(totalOutcome).format('0,0.00')}
            </Text>
            <Text>บาท</Text>
          </View>
        </View>
        <View style={styles.summaryContainer}>
          <Text style={styles.summaryTitle}>รวมรายได้สุทธิ</Text>
          <View style={styles.summaryValueContainer}>
            <Text
              style={[
                styles.summaryValue,
                { backgroundColor: Theme.GRAY_COLOR },
              ]}>
              {numeral(totalNetIncome).format('0,0.00')}
            </Text>
            <Text>บาท</Text>
          </View>
        </View>
      </View>
      <Divider style={{ height: 1.5 }} />
    </>
  );
};

const styles = StyleSheet.create({
  formMargin: { marginHorizontal: 15, marginVertical: 10 },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  summaryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  summaryTitle: {
    width: '25%',
  },
  summaryValueContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  summaryValue: {
    flex: 1,
    marginRight: 5,
    padding: 10,
    backgroundColor: Theme.GRAY_BORDER_COLOR,
    textAlign: 'right',
    fontSize: 15,
  },
});

export default FormSection4;
