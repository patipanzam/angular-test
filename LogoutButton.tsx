import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import { logout } from '../store/authSlice';

export default function LogoutButton() {
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLogout = () => {
    dispatch(logout());
    localStorage.removeItem('token');
    router.push('/login');
  };

  return <button onClick={handleLogout}>Logout</button>;
}
