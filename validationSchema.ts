import * as yup from 'yup';
import {
  IFormSection1,
  IFormSection2,
  IFormSection3,
  IFormSection4,
  IVerifyForm,
} from './interfaces';

export type FormSchema = {
  verifyForm: IVerifyForm;
  section1: IFormSection1;
  section2: IFormSection2;
  section3: IFormSection3;
  section4: IFormSection4;
};

const verifyForm = yup.object().shape({
  A0_C7: yup.string().required('กรุณาเลือก'),
  A0_C1: yup.string().required('กรุณาระบุสถานที่ที่เดินทางจาก'),
  A0_C2: yup.string().required('กรุณาระบุตัวเลขกิโลเมตรที่เดินทาง'),
  A0_C3: yup.string().required('กรุณาระบุสถานที่ที่เดินทางถึง'),
  A0_C4: yup
    .string()
    .required('กรุณาระบุตัวเลขกิโลเมตรที่เดินทางถึง')
    .when('A0_C2', (A0_C2, schema) => {
      return schema.test({
        test: (A0_C4: string) =>
          Number(A0_C2.replace(/,/g, '')) < Number(A0_C4.replace(/,/g, '')),
        message: 'เลขกิโลเมตรเดินทางถึงควรจะมากกว่าเลขเดินทางจาก',
      });
    }),
  A0_C8: yup.string().required('กรุณาเลือก'),
  A0_C9: yup.string().required('กรุณาระบุเหตุผลในกรณีที่ไม่พร้อมตรวจสอบ'),
  A0_C6: yup.string().required('กรุณาระบุความสัมพันธ์'),
  A9: yup.string().required('กรุณาเลือก'),
  A9_C2_1: yup.string().required('กรุณาระบุเหตุผลในกรณีที่ไม่ยืนยัน'),
});

const section1 = yup.object().shape({
  A1: yup.string().required('กรุณาระบุสถานที่ตั้งของที่พักอาศัย'),
  A2: yup.string().required('กรุณาระบุประเภทที่อยู่'),
  A2_C1_1: yup
    .string()
    .required('กรุณาระบุความสูง')
    .max(3, 'ต้องไม่เกิน 3 ตัวอักษร'),
  A2_C1_2: yup
    .string()
    .required('กรุณาระบุเนื้อที่')
    .max(6, 'ต้องไม่เกิน 6 ตัวอักษร'),
  A2_C2_1: yup
    .string()
    .required('กรุณาระบุความสูง')
    .max(3, 'ต้องไม่เกิน 3 ตัวอักษร'),
  A2_C2_2: yup
    .string()
    .required('กรุณาระบุเนื้อที่')
    .max(6, 'ต้องไม่เกิน 6 ตัวอักษร'),
  A2_C3_1: yup
    .string()
    .required('กรุณาระบุความสูง')
    .max(3, 'ต้องไม่เกิน 3 ตัวอักษร'),
  A2_C3_2: yup
    .string()
    .required('กรุณาระบุเนื้อที่')
    .max(6, 'ต้องไม่เกิน 6 ตัวอักษร'),
  A2_C7_1: yup
    .string()
    .required('กรุณาระบุค่าเช่าต่อเดือน')
    .max(10, 'ต้องไม่เกิน 10 ตัวอักษร'),
  A2_C8_1: yup
    .string()
    .required('กรุณาระบุค่าเช่าต่อปี')
    .max(10, 'ต้องไม่เกิน 10 ตัวอักษร'),
  A2_C9_1: yup.string().required('กรุณาระบุประเภทที่อยู่'),
  A2_C10: yup.string().required('กรุณาระบุข้อมูลรายละเอียดสถานที่พักอาศัย'),
  A2_C11: yup.string().required('กรุณาระบุสภาพเส้นทาง'),
  A2_C12: yup.string().required('กรุณาระบุความเจริญ'),
  A2_C13: yup.string().required('กรุณาระบุสถานที่จอดรถ'),
  A3: yup.string().required('กรุณาระบุสถานภาพของผู้อยู่อาศัย'),
  A3_C6_1: yup.string().required('กรุณาระบุบุคคลที่ไปพักอาศัย'),
  A3_C8_1: yup
    .string()
    .required('กรุณาระบุจำนวนปีที่เช่า/เซ้ง')
    .max(3, 'ต้องไม่เกิน 3 ตัวอักษร'),
  A3_C9_1: yup.string().required('กรุณาระบุสถานภาพการอยู่อาศัยอื่นๆ'),
  // A3_C10: yup
  //   .string()
  //   .required('กรุณาระบุปีอยู่ระหว่าง 0-99 ปี')
  //   .when('A3_C9_1', (_, schema) => {
  //     return schema.test({
  //       test: (A3_C10: string) => Number(A3_C10) <= 99,
  //       message: 'กรุณาระบุปีอยู่ระหว่าง 0-99 ปี',
  //     });
  //   }),
  // A3_C12: yup
  //   .string()
  //   .required('กรุณาระบุเดือนอยู่ระหว่าง 0-11 เดือน')
  //   .when('A3_C10', (_, schema) => {
  //     return schema.test({
  //       test: (A3_C12: string) => Number(A3_C12) <= 12,
  //       message: 'กรุณาระบุเดือนอยู่ระหว่าง 0-11 เดือน',
  //     });
  //   }),
  A3_C10: yup.string().optional(),
  A3_C12: yup.string().optional(),
  A3_C10_1: yup.string().required('กรุณาระบุที่พักอาศัยเก่า'),
  A3_C11_F1: yup.string().optional(),
  A3_C11_F2: yup.string().optional(),
  A3_C11_F3: yup.string().optional(),
  A3_C11_F4: yup.string().optional(),
  A3_C11_F5: yup.string().optional(),
  A3_C11_F6: yup.string().optional(),
  A3_C11_F7: yup.string().optional(),
});

const section2 = yup.object().shape({
  A4: yup.string().required('กรุณาระบุลักษณะที่ตั้งกิจการ'),
  A4_C1_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C2_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C3_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C4_1: yup.string().required('กรุณาระบุ').max(2, 'ต้องไม่เกิน 2 ตัวอักษร'),
  A4_C5_1: yup.string().required('กรุณาระบุ').max(2, 'ต้องไม่เกิน 2 ตัวอักษร'),
  A4_C7_1: yup.string().required('กรุณาระบุ').max(2, 'ต้องไม่เกิน 2 ตัวอักษร'),
  A4_C8_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C9_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C10_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C11_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C12_1: yup.string().required('กรุณาระบุ').max(9, 'ต้องไม่เกิน 9 ตัวอักษร'),
  A4_C17_1: yup.string().optional(),
  A4_C17_2: yup.string().optional(),
  A4_C21_1: yup.string().required('กรุณาระบุ'),
  A5: yup.string().required('กรุณาระบุข้อมูลระดับการสัญจร'),
});

const section3 = yup.object().shape({
  A6: yup.string().required('กรุณาระบุข้อมูลรูปแบบกิจการ'),
  A6_C4: yup.string().required('กรุณาระบุประเภทของธุรกิจหรือกิจการ'),
  A6_C5: yup.string().required('กรุณาระบุจำนวนพนักงาน'),
  A7_C1: yup.string().optional(),
  A7_C2: yup.string().optional(),
  A7_C3: yup.string().optional(),
  A7_C1_1: yup.string().required('กรุณาระบุกิจการเก่า'),
  A7_C4_F1: yup.string().optional(),
  A7_C4_F2: yup.string().optional(),
  A7_C4_F3: yup.string().optional(),
  A7_C4_F4: yup.string().optional(),
  A7_C4_F5: yup.string().optional(),
  A7_C4_F6: yup.string().optional(),
  A7_C4_F7: yup.string().required('กรุณาระบุ'),
  A10_C1: yup.string().optional(),
  A10_C1_1: yup.string().optional(),
  A10_C2: yup.string().optional(),
  A10_C2_1: yup.string().optional(),
  A10_C3: yup.string().optional(),
  A10_C3_1: yup.string().optional(),
});

const section4 = yup.object().shape({
  A19: yup.string().required('กรุณาระบุชื่อกิจการ'),
  // A11_C1: yup.string().required('กรุณาระบุยอดขายรายได้'),
  A11_C1: yup.string().optional(),
  A12_C1_1: yup.string().required('กรุณารายการต้นทุน'),
  // A12_C1_2: yup.string().required('กรุณาระบุจำนวนเงิน'),
  A12_C1_2: yup.string().optional(),
  A12_C2_1: yup.string().optional(),
  A12_C2_2: yup.string().optional(),
  A12_C3_1: yup.string().optional(),
  A12_C3_2: yup.string().optional(),
  A20: yup.string().optional(),
  A15_C1: yup.string().optional(),
  A16_C1_1: yup.string().optional(),
  A16_C1_2: yup.string().optional(),
  A16_C2_1: yup.string().optional(),
  A16_C2_2: yup.string().optional(),
  A16_C3_1: yup.string().optional(),
  A16_C3_2: yup.string().optional(),
  A21: yup.string().optional(),
  A17_C1: yup.string().optional(),
  A18_C1_1: yup.string().optional(),
  A18_C1_2: yup.string().optional(),
  A18_C2_1: yup.string().optional(),
  A18_C2_2: yup.string().optional(),
  A18_C3_1: yup.string().optional(),
  A18_C3_2: yup.string().optional(),
  A13: yup.string().optional(),
  A14_C1: yup.string().required('กรุณาระบุข้อมูลลูกค้า'),
  A14_C2: yup.string().optional(),
  A14_C3: yup.string().optional(),
  incomeFormQTY: yup.string().optional(),
});

export const formSchema: yup.SchemaOf<FormSchema> = yup.object({
  verifyForm,
  section1,
  section2,
  section3,
  section4,
});
