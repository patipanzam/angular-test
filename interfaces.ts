import { StyleProp, ViewStyle } from 'react-native';

export interface IForm
  extends IVerifyForm,
    IFormSection1,
    IFormSection2,
    IFormSection3,
    IFormSection4 {}

export interface IVerifyForm {
  // -- section verify --
  // for(home, business, home and business)
  verifyForm: {
    // สถานที่ที่ตรวจสอบ - radio
    A0_C7: '1' | '2' | '3';
    // เดินทางจาก - input
    A0_C1: string;
    // เลขกิโลเมตร - input number (0 - 999999)
    A0_C2: string;
    // เดินทางถึง - input
    A0_C3: string;
    // เลขกิโลเมตร - input number (0 - 999999)
    A0_C4: string;
    // ตรวจสอบบุคคลหรือสถานที่ตั้ง - radio
    A0_C8: '1' | '0';
    // กรุณาระบุเหตุผลในกรณีที่ไม่พร้อมตรวจสอบ - input
    A0_C9: string;
    // ความสัมพันธ์กับลูกค้า - input
    A0_C6: string;
    // ยืนยันการค้ำประกัน - radio
    A9: '' | 'C1' | 'C2';
    // กรุณาระบุเหตุผลในกรณีที่ไม่ยืนยัน - input
    A9_C2_1: string;
  };
}

export interface IFormSection1 {
  // -- section 1 -- ตรวจสอบสถานที่ตั้งของที่พักอาศัย
  // for(home, home and business)
  section1: {
    // 1. ตรวจสอบสถานที่ตั้งของที่พักอาศัย - select
    A1: '' | 'C1' | 'C2';
    // 2. ลักษณะของที่อยู่อาศัย
    // ประเภทที่อยู่ - select
    A2: '' | 'C1' | 'C2' | 'C3' | 'C4' | 'C5' | 'C6' | 'C7' | 'C8' | 'C9';
    // ความสูง (ชั้น) - input number maxlength 3
    A2_C1_1: string;
    // เนื้อที่ (ตรว.) - input number maxlength 6
    A2_C1_2: string;
    // ความสูง (ชั้น) - input number maxlength 3
    A2_C2_1: string;
    // เนื้อที่ (ตรว.) - input number maxlength 6
    A2_C2_2: string;
    // ความสูง (ชั้น) - input number maxlength 3
    A2_C3_1: string;
    // เนื้อที่ (ตรว.) - input number maxlength 6
    A2_C3_2: string;
    // เดือนละ (บาท) - input number maxlength 10
    A2_C7_1: string;
    // ปีละ (บาท) - input number maxlength 10
    A2_C8_1: string;
    // อื่นๆ (โปรดระบุ) - input
    A2_C9_1: string;
    // รายละเอียดสถานที่พักอาศัย - input
    A2_C10: string;
    // สภาพเส้นทาง - input
    A2_C11: string;
    // ความเจริญ - input
    A2_C12: string;
    // ระบุที่จอดรถ - input
    A2_C13: string;
    // 3. สถานภาพของผู้อยู่อาศัย
    // สถานภาพ - select
    A3: '' | 'C1' | 'C2' | 'C3' | 'C4' | 'C5' | 'C6' | 'C7' | 'C8' | 'C9';
    // อาศัยอยู่กับ (ไม่ใช่ญาติ) - input
    A3_C6_1: string;
    // สิทธิ์การเช่าระยะยาว/เซ้ง - จำนวน (ปี) - input number maxLength 3
    A3_C8_1: string;
    // อื่นๆ โปรดระบุ - input
    A3_C9_1: string;
    // ระยะเวลาที่พักอาศัย
    // ปี - input number, default 0, maxlength 2, range 0-99
    A3_C10: string;
    // เดือน - input number, default 0, maxlength 2, range 0-11
    A3_C12: string;
    // ถ้าน้อยกว่า 1 ปี กรุณาระบุที่พักอาศัยเก่า - input
    A3_C10_1: string;
    // ได้รับข้อมูลจาก
    // ผู้เช่าซื้อ - checkbox
    A3_C11_F1: '' | '1';
    // ผู้ค้ำประกัน - checkbox
    A3_C11_F2: '' | '1';
    // ร้านค้า - checkbox
    A3_C11_F3: '' | '1';
    // เพื่อนบ้าน - checkbox
    A3_C11_F4: '' | '1';
    // กิจการข้างเคียง - checkbox
    A3_C11_F5: '' | '1';
    // อื่นๆ โปรดระบุ - checkbox
    A3_C11_F6: '' | '1';
    // โปรดระบุ - input
    A3_C11_F7: string;
  };
}

export interface IFormSection2 {
  // -- section 2 -- ตรวจสอบสถานที่ตั้งของกิจการหรือสถานที่ตั้งของสถานประกอบการ
  // for(business, home and business)
  section2: {
    // ลักษณะที่ตั้งที่ทำงานหรือที่ตั้งกิจการ - select
    A4:
      | ''
      | 'C1'
      | 'C2'
      | 'C3'
      | 'C4'
      | 'C5'
      | 'C6'
      | 'C7'
      | 'C8'
      | 'C9'
      | 'C10'
      | 'C11'
      | 'C12'
      | 'C13'
      | 'C14'
      | 'C15'
      | 'C16'
      | 'C17'
      | 'C18'
      | 'C19'
      | 'C20'
      | 'C21';
    // ขนาด (ตรม.) - input number maxlength 6
    A4_C1_1: string;
    // ขนาด (ตรม.) - input number maxlength 6
    A4_C2_1: string;
    // ขนาด (ตรม.) - input number maxlength 6
    A4_C3_1: string;
    // จำนวน (ชั้น) - input number maxlength 2
    A4_C4_1: string;
    // จำนวน (ชั้น) - input number maxlength 2
    A4_C5_1: string;
    // จำนวน (ชั้น) - input number maxlength 2
    A4_C7_1: string;
    // ขนาด (ตร.ม.) - input number maxlength 6
    A4_C8_1: string;
    // ขนาด (ตร.ม.) - input number maxlength 6
    A4_C9_1: string;
    // ขนาด (ตร.ม.) - input number maxlength 6
    A4_C10_1: string;
    // ขนาด (ตร.ม.) - input number maxlength 6
    A4_C11_1: string;
    // ขนาด (ตร.ม.) - input number maxlength 6
    A4_C12_1: string;
    // ขนาด
    // ไร่ - input number maxlength 6, default 0
    A4_C17_1: string;
    // ตร.ว. - input number maxlength 6
    A4_C17_2: string;
    // โปรดระบุ - input
    A4_C21_1: string;
    // ระดับการสัญจรไปมาในละแวกที่ตั้งกิจการ - select
    A5: '' | 'C1' | 'C2' | 'C3';
  };
}

export interface IFormSection3 {
  // -- section 3 -- ตรวจสอบข้อมูลรายได้ในกรณีเจ้าของกิจการ
  // for(business, home and business)
  section3: {
    //การตรวจสอบข้อมูลกิจการ
    //รูปแบบของกิจการ - select
    A6: '' | 'C1' | 'C2' | 'C3' | 'C6';
    // ประเภทของธุรกิจ/กิจการ - input
    A6_C4: string;
    //จำนวนพนักงาน (คน) - input number, 0 - 99999, maxLenth 5
    A6_C5: string;
    // อายุของกิจการ
    // ปี - input number, 0 - 99, maxLength 2, default 0
    A7_C1: string;
    // เดือน - input number, 0 - 11 maxLength 2
    A7_C2: string;
    // ปิดกิจการ - checkbox
    A7_C3: '' | '1';
    // ถ้าน้อยกว่า 1 ปี กรุณาระบุกิจการเก่า - input
    A7_C1_1: string;
    // ได้รับข้อมูลจาก
    // ผู้เช่าซื้อ - checkbox
    A7_C4_F1: '' | '1';
    // ผู้ค้ำประกัน - checkbox
    A7_C4_F2: '' | '1';
    // ร้านค้า - checkbox
    A7_C4_F3: '' | '1';
    // เพื่อนบ้าน - checkbox
    A7_C4_F4: '' | '1';
    // กิจการข้างเคียง - checkbox
    A7_C4_F5: '' | '1';
    // อื่นๆ โปรดระบุ - checkbox
    A7_C4_F6: '' | '1';
    // โปรดระบุ - input
    A7_C4_F7: string;
    // ข้อมูลภาระหนี้สินที่สอบถามจากลูกค้า
    // มีผ่อนบ้าน/ที่ดิน - checkbox
    A10_C1: '' | '1';
    // เดือนละ - input number, currency, maxLength 10
    A10_C1_1: string;
    // มีผ่อนรถ - checkbox
    A10_C2: '' | '1';
    // เดือนละ - input number, currency, maxLength 10
    A10_C2_1: string;
    // สินเชื่ออื่นๆ - checkbox
    A10_C3: '' | '1';
    // เดือนละ - input number, currency, maxLength 10
    A10_C3_1: string;
  };
}

export interface IFormSection4 {
  // -- section 4 -- รายละเอียดรายได้และกิจการ (โปรดระบุข้อมูลลูกค้า)
  // for(home, business, home and business)
  section4: {
    // รายละเอียดรายได้/รายรับในกิจการที่ 1

    // ชื่อกิจการ - input
    A19: string;
    // ยอดขายรายได้ (บาท/เดือน) - input, currency
    A11_C1: string;
    // รายละเอียดต้นทุน - input
    // ต้นทุน
    A12_C1_1: string;
    // จำนวน (บาท/เดือน) - input currency
    A12_C1_2: string;
    // ต้นทุน - input
    A12_C2_1: string;
    // จำนวน (บาท/เดือน) - input currency
    A12_C2_2: string;
    // ต้นทุน - input
    A12_C3_1: string;
    // จำนวน (บาท/เดือน) - input currency
    A12_C3_2: string;

    // รายละเอียดรายได้/รายรับในกิจการที่ 2 (ถ้ามี)
    A20: string;
    A15_C1: string;
    A16_C1_1: string;
    A16_C1_2: string;
    A16_C2_1: string;
    A16_C2_2: string;
    A16_C3_1: string;
    A16_C3_2: string;

    // รายละเอียดรายได้/รายรับในกิจการที่ 3 (ถ้ามี)
    A21: string;
    A17_C1: string;
    A18_C1_1: string;
    A18_C1_2: string;
    A18_C2_1: string;
    A18_C2_2: string;
    A18_C3_1: string;
    A18_C3_2: string;

    // ข้อมูลเพิ่มเติม - textarea
    A13: string;
    // ข้อมูลลูกค้า (โปรดระบุ) - textarea
    A14_C1: string;
    // ข้อมูลข้างเคียง - textarea
    A14_C2: string;
    // ข้อมูลรายได้ - textarea
    A14_C3: string;
    // dynamic form income outcome
    incomeFormQTY: number;
  };
}

export interface IFormList {
  index: number;
  value: string;
  label: string;
  style?: StyleProp<ViewStyle>;
}

export interface IRadio extends IFormList {}
export interface ISelect extends IFormList {}

export interface IRadioList {
  A0_C7: IRadio[];
  A0_C8: IRadio[];
  A9: IRadio[];
}

export interface ISelectList {
  A1: ISelect[];
  A2: ISelect[];
  A3: ISelect[];
  A4: ISelect[];
  A5: ISelect[];
  A6: ISelect[];
}
