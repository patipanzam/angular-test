import React from 'react';
import { Radio as UKRadio, RadioGroup } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import Theme from '../Theme';
import { IRadio } from './VerifyForm';

interface IRadioProps {
  items: IRadio[];
  value: string;
  onSelect: (value: string | undefined) => void;
}

{
  /* TODO: selected background color */
}
const Radio: React.FC<IRadioProps> = ({ items, value, onSelect }) => {
  const findItem = (by: keyof IRadio, value: string | number) => {
    return items.find((item) => item[by] === value);
  };

  return (
    <RadioGroup
      selectedIndex={findItem('value', value)?.index}
      onChange={(index) => onSelect(findItem('index', index)?.value)}
    >
      {items.map((item, index) => (
        <UKRadio
          status={'primary'}
          key={item.value}
          style={[
            styles.radio,
            item.style,
            index !== 0 && { borderTopWidth: 0 },
          ]}
        >
          {item.label}
        </UKRadio>
      ))}
    </RadioGroup>
  );
};

const styles = StyleSheet.create({
  radio: {
    backgroundColor: 'white',
    padding: 10,
    borderWidth: 1,
    borderColor: Theme.PRIMARY_COLOR,
    marginTop: 0,
    marginBottom: 0,
  },
});

export default Radio;
