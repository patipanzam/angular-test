import React from 'react';
import Image from 'next/image';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { logout } from '../store/authSlice';
import { RootState } from '../store/store';

const Header: React.FC = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const username = useSelector((state: RootState) => state.auth.username);

  const handleLogout = () => {
    dispatch(logout());
    localStorage.removeItem('token');
    router.push('/');
  };

  return (
    <header className="bg-gray-800 text-white shadow-md py-4 px-6 flex justify-between items-center">
      {/* Logo Section */}
      <div className="flex items-center">
        <Image src="/logo.png" alt="Logo" width={40} height={40} className="mr-2" />
        <h1 className="text-xl font-bold">MyApp</h1>
      </div>

      {/* User Section */}
      <div className="flex items-center space-x-4">
        <Image
          src="/user-icon.png" // Placeholder for user icon
          alt="User Icon"
          width={32}
          height={32}
          className="rounded-full"
        />
        <span className="text-lg">{username}</span>
        <button
          onClick={handleLogout}
          className="bg-red-500 text-white py-1 px-3 rounded-md hover:bg-red-600 transition duration-300"
        >
          Logout
        </button>
      </div>
    </header>
  );
};

export default Header;
