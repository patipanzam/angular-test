
// services/api.ts
import axiosInstance from './axiosInstance';

export const getDataWithAxios = async (endpoint: string) => {
  try {
    const response = await axiosInstance.get(endpoint);
    return response.data;
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }
};

// services/api.ts
export const postDataWithAxios = async (endpoint: string, body: any) => {
  try {
    const response = await axiosInstance.post(endpoint, body);
    return response.data;
  } catch (error) {
    console.error('Error posting data:', error);
    throw error;
  }
};



// pages/example.tsx
import React, { useEffect, useState } from 'react';
import { getDataWithAxios, postDataWithAxios } from '../services/api';

const ExamplePageWithAxios: React.FC = () => {
  const [data, setData] = useState<any>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getDataWithAxios('/data');
        setData(response);
      } catch (err) {
        setError(err.message);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const handleSubmit = async () => {
    const payload = { key: 'value' }; // Replace with actual data
    try {
      const response = await postDataWithAxios('/submit', payload);
      console.log('POST response:', response);
    } catch (err) {
      console.error('Error submitting data:', err);
    }
  };

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;

  return (
    <div>
      <h1>Data:</h1>
      <pre>{JSON.stringify(data, null, 2)}</pre>
      <button onClick={handleSubmit}>Submit Data</button>
    </div>
  );
};

export default ExamplePageWithAxios;





