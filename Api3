import React, { useState, useEffect, useRef } from 'react';
import * as pdfjsLib from 'pdfjs-dist';

const FileUploadAndViewer = () => {
  const [files, setFiles] = useState<
    { id: string; name: string; type: string; url: string; pages?: number }[]
  >([]);
  const [selectedFileIndex, setSelectedFileIndex] = useState<number | null>(null);
  const [currentPage, setCurrentPage] = useState(1);
  const containerRef = useRef<HTMLDivElement>(null);

  // Set up PDF.js worker
  pdfjsLib.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjsLib.version}/pdf.worker.min.js`;

  const renderPDF = async (pdfUrl: string) => {
    const pdf = await pdfjsLib.getDocument(pdfUrl).promise;
    const numPages = pdf.numPages;

    if (containerRef.current) {
      containerRef.current.innerHTML = ''; // Clear previous renders
    }

    for (let i = 1; i <= numPages; i++) {
      const page = await pdf.getPage(i);
      const viewport = page.getViewport({ scale: 1.5 });

      // Create canvas for the page
      const canvas = document.createElement('canvas');
      canvas.width = viewport.width;
      canvas.height = viewport.height;

      const context = canvas.getContext('2d');
      if (context) {
        const renderContext = {
          canvasContext: context,
          viewport: viewport,
        };
        await page.render(renderContext).promise;
      }

      // Append canvas to the container
      containerRef.current?.appendChild(canvas);
    }

    // Return the number of pages
    return numPages;
  };

  const handleFileUpload = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const uploadedFiles = Array.from(event.target.files || []);
    const fileList = await Promise.all(
      uploadedFiles.map(async (file) => {
        let pages: number | undefined;

        if (file.type === 'application/pdf') {
          try {
            const pdf = await pdfjsLib.getDocument(URL.createObjectURL(file)).promise;
            pages = pdf.numPages;
          } catch (error) {
            console.error('Error loading PDF:', error);
            pages = undefined;
          }
        }

        return {
          id: crypto.randomUUID(),
          name: file.name,
          type: file.type,
          url: URL.createObjectURL(file),
          pages,
        };
      })
    );

    setFiles((prevFiles) => [...prevFiles, ...fileList]);
    if (selectedFileIndex === null && fileList.length > 0) {
      setSelectedFileIndex(0);
    }
  };

  const handleFileSelect = (index: number) => {
    setSelectedFileIndex(index);
    setCurrentPage(1); // Reset to page 1 on file change
  };

  const handleScroll = () => {
    if (containerRef.current) {
      const scrollTop = containerRef.current.scrollTop;
      const pageElements = containerRef.current.querySelectorAll('canvas');
      const containerHeight = containerRef.current.clientHeight;

      let newCurrentPage = 1;

      pageElements.forEach((page, index) => {
        const pageTop = page.getBoundingClientRect().top - containerRef.current!.getBoundingClientRect().top;
        const pageHeight = page.clientHeight;

        if (pageTop <= containerHeight / 2 && pageTop + pageHeight > containerHeight / 2) {
          newCurrentPage = index + 1;
        }
      });

      if (newCurrentPage !== currentPage) {
        setCurrentPage(newCurrentPage);
      }
    }
  };

  useEffect(() => {
    if (selectedFileIndex !== null && files[selectedFileIndex]?.url) {
      renderPDF(files[selectedFileIndex].url);
    }
  }, [selectedFileIndex]);

  return (
    <div style={{ padding: '20px', height: '100vh', overflow: 'hidden' }}>
      <h2>PDF Viewer</h2>
      <input
        type="file"
        accept=".pdf"
        multiple
        onChange={handleFileUpload}
        style={{ marginBottom: '20px' }}
      />
      <div
        style={{
          border: '1px solid #ccc',
          height: '70vh',
          display: 'flex',
          flexDirection: 'column',
          position: 'relative',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '10px',
            borderBottom: '1px solid #ccc',
            backgroundColor: '#f8f9fa',
          }}
        >
          <button onClick={() => setSelectedFileIndex(null)}>&larr; Back</button>
          {selectedFileIndex !== null && files[selectedFileIndex] && (
            <div>
              <strong>{files[selectedFileIndex].name}</strong>
              <span style={{ marginLeft: '10px' }}>
                ({currentPage}/{files[selectedFileIndex]?.pages || 'Unknown'})
              </span>
            </div>
          )}
        </div>
        <div
          ref={containerRef}
          onScroll={handleScroll}
          style={{
            flex: 1,
            overflowY: 'scroll',
            padding: '10px',
            backgroundColor: '#f8f9fa',
          }}
        />
      </div>
    </div>
  );
};

export default FileUploadAndViewer;


how to modify this code to PdfGallery and select one page to new file



import React, { useState, useEffect, useRef } from 'react';
import * as pdfjsLib from 'pdfjs-dist';
import { PDFDocument } from 'pdf-lib';

const PdfGallery = () => {
  const [files, setFiles] = useState<
    { id: string; name: string; type: string; url: string; pages?: number }[]
  >([]);
  const [selectedFileIndex, setSelectedFileIndex] = useState<number | null>(null);
  const [selectedPage, setSelectedPage] = useState<number | null>(null);
  const [pdfBlob, setPdfBlob] = useState<Blob | null>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  // Set up PDF.js worker
  pdfjsLib.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjsLib.version}/pdf.worker.min.js`;

  const renderPDF = async (pdfUrl: string) => {
    const pdf = await pdfjsLib.getDocument(pdfUrl).promise;
    const numPages = pdf.numPages;
    if (containerRef.current) {
      containerRef.current.innerHTML = ''; // Clear previous renders
    }
    for (let i = 1; i <= numPages; i++) {
      const page = await pdf.getPage(i);
      const viewport = page.getViewport({ scale: 1.5 });
      // Create canvas for the page
      const canvas = document.createElement('canvas');
      canvas.width = viewport.width;
      canvas.height = viewport.height;
      const context = canvas.getContext('2d');
      if (context) {
        const renderContext = {
          canvasContext: context,
          viewport: viewport,
        };
        await page.render(renderContext).promise;
      }
      // Append canvas to the container
      canvas.style.border = '1px solid #ccc';
      canvas.style.marginBottom = '10px';
      canvas.style.cursor = 'pointer';
      canvas.onclick = () => handlePageSelect(i);
      containerRef.current?.appendChild(canvas);
    }
    return numPages;
  };

  const handleFileUpload = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const uploadedFiles = Array.from(event.target.files || []);
    const fileList = await Promise.all(
      uploadedFiles.map(async (file) => {
        let pages: number | undefined;
        if (file.type === 'application/pdf') {
          try {
            const pdf = await pdfjsLib.getDocument(URL.createObjectURL(file)).promise;
            pages = pdf.numPages;
          } catch (error) {
            console.error('Error loading PDF:', error);
            pages = undefined;
          }
        }
        return {
          id: crypto.randomUUID(),
          name: file.name,
          type: file.type,
          url: URL.createObjectURL(file),
          pages,
        };
      })
    );
    setFiles((prevFiles) => [...prevFiles, ...fileList]);
    if (selectedFileIndex === null && fileList.length > 0) {
      setSelectedFileIndex(0);
    }
  };

  const handleFileSelect = (index: number) => {
    setSelectedFileIndex(index);
    setSelectedPage(null); // Reset selected page
  };

  const handlePageSelect = async (pageNumber: number) => {
    setSelectedPage(pageNumber);

    if (selectedFileIndex !== null && files[selectedFileIndex]?.url) {
      const response = await fetch(files[selectedFileIndex].url);
      const originalPdfBytes = await response.arrayBuffer();
      const pdfDoc = await PDFDocument.load(originalPdfBytes);

      // Create a new PDF with the selected page
      const newPdfDoc = await PDFDocument.create();
      const [copiedPage] = await newPdfDoc.copyPages(pdfDoc, [pageNumber - 1]); // Pages are 0-indexed
      newPdfDoc.addPage(copiedPage);

      const newPdfBytes = await newPdfDoc.save();
      const newPdfBlob = new Blob([newPdfBytes], { type: 'application/pdf' });
      setPdfBlob(newPdfBlob);
    }
  };

  const downloadNewPdf = () => {
    if (pdfBlob && selectedPage) {
      const link = document.createElement('a');
      link.href = URL.createObjectURL(pdfBlob);
      link.download = `page-${selectedPage}.pdf`;
      link.click();
    }
  };

  useEffect(() => {
    if (selectedFileIndex !== null && files[selectedFileIndex]?.url) {
      renderPDF(files[selectedFileIndex].url);
    }
  }, [selectedFileIndex]);

  return (
    <div style={{ padding: '20px', height: '100vh', overflow: 'hidden' }}>
      <h2>PDF Gallery</h2>
      <input
        type="file"
        accept=".pdf"
        multiple
        onChange={handleFileUpload}
        style={{ marginBottom: '20px' }}
      />
      <div
        style={{
          border: '1px solid #ccc',
          height: '70vh',
          display: 'flex',
          flexDirection: 'column',
          position: 'relative',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '10px',
            borderBottom: '1px solid #ccc',
            backgroundColor: '#f8f9fa',
          }}
        >
          <button onClick={() => setSelectedFileIndex(null)}>&larr; Back</button>
          {selectedFileIndex !== null && files[selectedFileIndex] && (
            <div>
              <strong>{files[selectedFileIndex].name}</strong>
              <span style={{ marginLeft: '10px' }}>
                ({files[selectedFileIndex]?.pages || 'Unknown'} pages)
              </span>
            </div>
          )}
        </div>
        <div
          ref={containerRef}
          style={{
            flex: 1,
            overflowY: 'scroll',
            padding: '10px',
            backgroundColor: '#f8f9fa',
          }}
        />
      </div>
      {selectedPage !== null && pdfBlob && (
        <div style={{ marginTop: '20px', textAlign: 'center' }}>
          <button
            onClick={downloadNewPdf}
            style={{
              padding: '10px 20px',
              backgroundColor: '#007bff',
              color: 'white',
              border: 'none',
              cursor: 'pointer',
              borderRadius: '4px',
            }}
          >
            Download Page {selectedPage} as New PDF
          </button>
        </div>
      )}
    </div>
  );
};

export default PdfGallery;


import React, { useState, useEffect, useRef } from 'react';
import * as pdfjsLib from 'pdfjs-dist';
import { PDFDocument } from 'pdf-lib';

const PdfGallery = () => {
  const [files, setFiles] = useState<
    { id: string; name: string; type: string; url: string; pages?: number }[]
  >([]);
  const [selectedFileIndex, setSelectedFileIndex] = useState<number | null>(null);
  const [selectedPages, setSelectedPages] = useState<number[]>([]);
  const [pdfBlob, setPdfBlob] = useState<Blob | null>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  // Set up PDF.js worker
  pdfjsLib.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjsLib.version}/pdf.worker.min.js`;

  const renderPDF = async (pdfUrl: string) => {
    const pdf = await pdfjsLib.getDocument(pdfUrl).promise;
    const numPages = pdf.numPages;
    if (containerRef.current) {
      containerRef.current.innerHTML = ''; // Clear previous renders
    }
    for (let i = 1; i <= numPages; i++) {
      const page = await pdf.getPage(i);
      const viewport = page.getViewport({ scale: 1.5 });
      // Create canvas for the page
      const canvas = document.createElement('canvas');
      canvas.width = viewport.width;
      canvas.height = viewport.height;
      const context = canvas.getContext('2d');
      if (context) {
        const renderContext = {
          canvasContext: context,
          viewport: viewport,
        };
        await page.render(renderContext).promise;
      }
      // Append canvas to the container
      canvas.style.border = selectedPages.includes(i) ? '2px solid blue' : '1px solid #ccc';
      canvas.style.marginBottom = '10px';
      canvas.style.cursor = 'pointer';
      canvas.onclick = () => handlePageSelect(i);
      containerRef.current?.appendChild(canvas);
    }
    return numPages;
  };

  const handleFileUpload = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const uploadedFiles = Array.from(event.target.files || []);
    const fileList = await Promise.all(
      uploadedFiles.map(async (file) => {
        let pages: number | undefined;
        if (file.type === 'application/pdf') {
          try {
            const pdf = await pdfjsLib.getDocument(URL.createObjectURL(file)).promise;
            pages = pdf.numPages;
          } catch (error) {
            console.error('Error loading PDF:', error);
            pages = undefined;
          }
        }
        return {
          id: crypto.randomUUID(),
          name: file.name,
          type: file.type,
          url: URL.createObjectURL(file),
          pages,
        };
      })
    );
    setFiles((prevFiles) => [...prevFiles, ...fileList]);
    if (selectedFileIndex === null && fileList.length > 0) {
      setSelectedFileIndex(0);
    }
  };

  const handleFileSelect = (index: number) => {
    setSelectedFileIndex(index);
    setSelectedPages([]); // Reset selected pages
  };

  const handlePageSelect = (pageNumber: number) => {
    setSelectedPages((prev) =>
      prev.includes(pageNumber)
        ? prev.filter((page) => page !== pageNumber) // Deselect if already selected
        : [...prev, pageNumber].sort((a, b) => a - b) // Add to selected pages
    );
  };

  const generateNewPdf = async () => {
    if (selectedFileIndex !== null && files[selectedFileIndex]?.url) {
      const response = await fetch(files[selectedFileIndex].url);
      const originalPdfBytes = await response.arrayBuffer();
      const pdfDoc = await PDFDocument.load(originalPdfBytes);

      const newPdfDoc = await PDFDocument.create();
      for (const pageNumber of selectedPages) {
        const [copiedPage] = await newPdfDoc.copyPages(pdfDoc, [pageNumber - 1]); // Pages are 0-indexed
        newPdfDoc.addPage(copiedPage);
      }

      const newPdfBytes = await newPdfDoc.save();
      const newPdfBlob = new Blob([newPdfBytes], { type: 'application/pdf' });
      setPdfBlob(newPdfBlob);
    }
  };

  const downloadNewPdf = () => {
    if (pdfBlob) {
      const link = document.createElement('a');
      link.href = URL.createObjectURL(pdfBlob);
      link.download = `selected-pages.pdf`;
      link.click();
    }
  };

  useEffect(() => {
    if (selectedFileIndex !== null && files[selectedFileIndex]?.url) {
      renderPDF(files[selectedFileIndex].url);
    }
  }, [selectedFileIndex, selectedPages]);

  return (
    <div style={{ padding: '20px', height: '100vh', overflow: 'hidden' }}>
      <h2>PDF Gallery</h2>
      <input
        type="file"
        accept=".pdf"
        multiple
        onChange={handleFileUpload}
        style={{ marginBottom: '20px' }}
      />
      <div
        style={{
          border: '1px solid #ccc',
          height: '70vh',
          display: 'flex',
          flexDirection: 'column',
          position: 'relative',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            padding: '10px',
            borderBottom: '1px solid #ccc',
            backgroundColor: '#f8f9fa',
          }}
        >
          <button onClick={() => setSelectedFileIndex(null)}>&larr; Back</button>
          {selectedFileIndex !== null && files[selectedFileIndex] && (
            <div>
              <strong>{files[selectedFileIndex].name}</strong>
              <span style={{ marginLeft: '10px' }}>
                ({files[selectedFileIndex]?.pages || 'Unknown'} pages)
              </span>
            </div>
          )}
        </div>
        <div
          ref={containerRef}
          style={{
            flex: 1,
            overflowY: 'scroll',
            padding: '10px',
            backgroundColor: '#f8f9fa',
          }}
        />
      </div>
      <div style={{ marginTop: '20px' }}>
        <h3>Selected Pages</h3>
        <div
          style={{
            display: 'flex',
            overflowX: 'scroll',
            gap: '10px',
            padding: '10px',
            border: '1px solid #ccc',
            backgroundColor: '#f8f9fa',
          }}
        >
          {selectedPages.map((page) => (
            <div key={page} style={{ textAlign: 'center' }}>
              <div
                style={{
                  width: '100px',
                  height: '140px',
                  border: '1px solid #ccc',
                  backgroundColor: '#ddd',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  fontSize: '16px',
                  fontWeight: 'bold',
                }}
              >
                Page {page}
              </div>
            </div>
          ))}
        </div>
      </div>
      {selectedPages.length > 0 && (
        <div style={{ marginTop: '20px', textAlign: 'center' }}>
          <button
            onClick={generateNewPdf}
            style={{
              marginRight: '10px',
              padding: '10px 20px',
              backgroundColor: '#28a745',
              color: 'white',
              border: 'none',
              cursor: 'pointer',
              borderRadius: '4px',
            }}
          >
            Generate New PDF
          </button>
          {pdfBlob && (
            <button
              onClick={downloadNewPdf}
              style={{
                padding: '10px 20px',
                backgroundColor: '#007bff',
                color: 'white',
                border: 'none',
                cursor: 'pointer',
                borderRadius: '4px',
              }}
            >
              Download New PDF
            </button>
          )}
        </div>
      )}
    </div>
  );
};

export default PdfGallery;









