import _ from 'lodash';
import { LOG } from '../Logs';

/**
 * Verify form count the make sure that save and sending form data is correct
 *
 * @param {string} formData serialized data from form.serialize();
 * @param {number} compareWith count that need to compare when produce finish result
 * @return {boolean|number} total form count if valid form otherwise false if form data is not correct
 */
export const verifyFormCount = async (
  formData: { [key: string]: string | null },
  compareWith?: number,
): Promise<number | undefined> => {
  let data: any | undefined;
  let count = 0;

  // sections
  const sections: any[] = [];
  const sectionsCount: any[] = [];

  // shorthand for is valid field
  const isValid = function isv(value: any) {
    console.log('SUBMIT : COUNT : check if ' + value + ' is empty?');
    LOG.debug('SUBMIT : COUNT : check if ' + value + ' is empty?');

    // trim if string
    // if (value && _.isString(value)) {
    //     value = $.trim(value);
    // }

    return value && !_.isEmpty(value) && !_.isNull(value);
  };

  sections[0] = ['A0_C1', 'A0_C2', 'A0_C3', 'A0_C4', 'A0_C7', 'A0_C8', 'A9'];

  sections[1] = [
    'A1',
    'A2',
    'A2_C10',
    'A2_C11',
    'A2_C12',
    'A2_C13',
    'A3',
    'A3_C10',
    [
      'A3_C11_F1',
      'A3_C11_F2',
      'A3_C11_F3',
      'A3_C11_F4',
      'A3_C11_F5',
      'A3_C11_F6',
      'A3_C11_F7',
    ],
    'A3_C12',
  ];

  sections[2] = ['A4', 'A5'];

  sections[3] = [
    'A6',
    'A6_C4',
    'A6_C5',
    'A7_C1',
    'A7_C2',
    'A7_C3',
    ['A7_C4_F1', 'A7_C4_F2', 'A7_C4_F3', 'A7_C4_F4', 'A7_C4_F5', 'A7_C4_F6'],
    ['A10_C1', 'A10_C2', 'A10_C3'],
  ];

  sections[4] = [
    'A19',
    'A11_C1',
    'A12_C1_1',
    'A12_C1_2',
    'A20',
    'A15_C1',
    'A16_C1_1',
    'A16_C1_2',
    'A21',
    'A17_C1',
    'A18_C1_1',
    'A18_C1_2',
    'A13',
    'A14_C1',
    'A14_C2',
    'A14_C3',
  ];

  sectionsCount[0] = 0;
  sectionsCount[1] = 0;
  sectionsCount[2] = 0;
  sectionsCount[3] = 0;
  sectionsCount[4] = 0;

  if (_.isNull(formData) || _.isEmpty(formData)) {
    return undefined;
  } else {
    data = formData;

    console.log('SUBMIT : Data = ' + data);
    LOG.debug('SUBMIT : Data = ' + data);

    // from section[] -> section[0..4]
    for (let i = 0, len = sections.length; i < len; i++) {
      const section = sections[i];

      //section[0..4] --> [A, B, C, ...]
      _.each(section, function (field) {
        console.log('SUBMIT : Check field = ' + field);
        LOG.debug('SUBMIT : Check field = ' + field);

        // field maybe [X,Y,Z]
        if (_.isArray(field)) {
          console.log('SUBMIT : Check array field = ' + field);
          LOG.debug('SUBMIT : Check array field = ' + field);

          // check if only have one of them
          // field = [X, Y, Z]
          _.each(field, function (subField) {
            if (isValid(data[subField])) {
              console.log(
                'SUBMIT : -- Count subfield = ' +
                  subField +
                  ' value = ' +
                  data[subField],
              );
              LOG.debug(
                'SUBMIT : -- Count subfield = ' +
                  subField +
                  ' value = ' +
                  data[subField],
              );
              sectionsCount[i]++;
              return false;
            }
          });
        } else {
          if (isValid(data[field])) {
            console.log(
              'SUBMIT : -- Count field = ' + field + ' value = ' + data[field],
            );
            LOG.debug(
              'SUBMIT : -- Count field = ' + field + ' value = ' + data[field],
            );
            sectionsCount[i]++;
          }
        }
      });

      console.log('SUBMIT : -- Count #' + i + ' = ' + sectionsCount[i]);
      LOG.debug('SUBMIT : -- Count #' + i + ' = ' + sectionsCount[i]);
    }

    // summary
    for (let i = 0, len = sectionsCount.length; i < len; i++) {
      count += sectionsCount[i];
    }

    console.log('SUBMIT : -- Total count = ' + count);
    LOG.debug('SUBMIT : -- Total count = ' + count);
    return count;
  }
};
