import React from 'react';
import Layout from '../components/Layout';

const Dashboard: React.FC = () => {
  return (
    <Layout>
      <h2 className="text-3xl font-bold mb-6">Dashboard</h2>
      <p>Welcome to the dashboard!</p>
      {/* Add more dashboard content here */}
    </Layout>
  );
};

export default Dashboard;
